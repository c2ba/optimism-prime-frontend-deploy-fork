import { ethers } from 'ethers'
import { TOKEN_ABI } from '../../utils/constants'

export async function getAllowance({
  owner,
  spender,
  signer,
  customAbi,
}: {
  owner: string
  spender: string
  signer: ethers.Signer
  customAbi?: any
}) {
  const signerAddress = await signer.getAddress()
  const contract = new ethers.Contract(
    owner,
    customAbi ? customAbi : TOKEN_ABI,
    signer,
  )

  try {
    const allowance = await contract.allowance(signerAddress, spender)
    return Number(allowance.toString()) > 0
  } catch (e) {
    return false
  }
}
