import { useCallback } from 'react';

type Props = {
  children?: any;
  buttonTitle: string;
  isOpen: boolean;
  clickAction: any;
};

export default function BlockModal({
  children,
  buttonTitle,
  isOpen,
  clickAction,
}: Props) {
  const onConfirm = useCallback(() => {
    clickAction();
  }, [clickAction]);

  return isOpen ? (
    <div className="fixed top-0 left-0 right-0 bottom-0 w-full h-screen z-50 overflow-hidden bg-black bg-opacity-40 flex flex-col items-center justify-center backdrop-blur-[16px] transition-opacity">
      <div className="w-[320px] sm:w-[400px] sm:mt-[-250px] bg-black/90 p-[24px] rounded-xl shadow-default text-white">
        {children}
        <button
          onClick={onConfirm}
          className={`flex flex-col h-20 w-full justify-center items-center px-3 py-2 rounded-md text-3xl transition ease-in-out delay-150 hover:enabled:-translate-y-1 hover:enabled:scale-[1.02] hover:enabled:bg-blue-600 hover:enabled:shadow-lg duration-300 bg-blue-700 opacity-100 text-white font-transformers`}
        >
          {buttonTitle}
        </button>
      </div>
    </div>
  ) : (
    <></>
  );
}
