export default function error() {
  return (
    <div className="flex flex-col w-full h-full justify-center items-center font-transformers space-y-10 my-20">
      <h1 className="text-5xl text-white">Page not found...</h1>
    </div>
  );
}
