import { ethers } from 'ethers'
import { TOKEN_ABI } from '../../utils/constants'
import type { Token } from '../../utils/types'

export async function getBalance(token: Token, signer: ethers.Signer) {
  try {
    const signerAddress = await signer.getAddress()
    if (token.title === 'ETH') {
      const balance = await signer.getBalance()
      return Number(ethers.utils.formatEther(balance))
    }

    const tokenContract = new ethers.Contract(
      token.contract as string,
      TOKEN_ABI,
      signer,
    )
    const balance = await tokenContract.balanceOf(signerAddress)
    return Number(ethers.utils.formatUnits(balance, token.decimals))
  } catch (e) {
    // console.log(e)
    return 0
  }
}
