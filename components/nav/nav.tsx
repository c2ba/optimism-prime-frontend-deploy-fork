import Link from 'next/link';
import { ConnectButton } from '@rainbow-me/rainbowkit';
import Image from 'next/image';
import Twitter from '../svg/twitter';
import Discord from '../svg/discord';
import Telegram from '../svg/telegram';
import Navbar from './navbar';

export default function Nav() {
  return (
    <nav className="hidden xl:flex justify-center px-4 md:px-12">
      <div className="flex flex-col sm:flex-row justify-between py-3 flex-1 max-w-[1920px]">
        <Link href="/">
          <a>
            <div className="flex flex-row justify-center sm:justify-start items-center font-transformers text-white text-4xl">
              <Image src="/logo_round.png" alt="logo" width={96} height={96} />
              <div className="mt-[8px] ml-[15px]">
                <span className="italic text-black mr-[10px]">OPTIMISM</span>
                <span>PRIME</span>
              </div>
            </div>
          </a>
        </Link>
        <div className="flex flex-row justify-center sm:justify-between items-center space-x-5 z-30">
          <Navbar
            styleFirstLink="text-white text-md xl:text-lg font-roboto-cond hover:opacity-75 transition ease-in-out"
            styleFirstLinkTwo="border-b-2"
            styleSecondLink="flex justify-center items-center text-white bg-blue-900 py-2  md:text-md xl:py-3 xl:px-6 xl:text-lg font-roboto-cond  shadow-xl rounded-md transition ease-in-out hover:scale-[1.03]"
          />
          <ConnectButton
            chainStatus="icon"
            accountStatus="address"
            label="CONNECT"
          />
          <div className="flex flex-row justify-center items-center space-x-2">
            <Link href="https://twitter.com/Optimism_Pr">
              <a target="blank" className="w-8 hover:opacity-70">
                <Twitter />
              </a>
            </Link>
            <Link href="https://discord.gg/SpfdM2VwUy">
              <a target="blank" className="w-8 hover:opacity-70">
                <Discord />
              </a>
            </Link>
            <Link href="https://t.me/OptimismPrimePortal">
              <a target="blank" className="w-8 hover:opacity-70">
                <Telegram />
              </a>
            </Link>
          </div>
        </div>
      </div>
    </nav>
  );
}
